package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import model.dao.ProfileDAO;

public class Profile implements Serializable {

	private static final long serialVersionUID = 7472295465482038837L;

	public enum Move {
		ROCK, PAPER, SCISSORS
	}

	private String name;
	private boolean active;
	private HashMap<Move, Integer> movePercent;
	private HashMap<String, LinkedList<Profile.Move>> oponentMovesMap;

	public Profile() {
		this("");
	}

	public Profile(String name) {
		this.name = name;
		this.active = true;

		this.movePercent = new HashMap<Move, Integer>();
		
		Random random = new Random();

		int preference = random.nextInt(3) + 1;
		int rockPercent = 0, paperPercent = 0, scissorsPercent = 0;
		
		switch (preference) {
		case 1:
			rockPercent = 60;
			paperPercent = 10;
			scissorsPercent = 30;
			break;
		case 2:
			rockPercent = 30;
			paperPercent = 60;
			scissorsPercent = 10;
			break;
		case 3:
			rockPercent = 10;
			paperPercent = 30;
			scissorsPercent = 60;
			break;

		default:
			break;
		}
		
		this.movePercent.put(Move.ROCK, rockPercent);
		this.movePercent.put(Move.PAPER, paperPercent);
		this.movePercent.put(Move.SCISSORS, scissorsPercent);

		this.oponentMovesMap = new HashMap<String, LinkedList<Move>>();
	}

	public void save() {
		ProfileDAO.getInstance().save(this);
		Profile.notifyOnSave(this);
	}

	public void destroy() {
		ProfileDAO.getInstance().destroy(this);
		Profile.notifyOnDestroy(this);
	}

	public Move makeMove(Profile oponent) {
		return makeMove(oponent.getName());
	}
	
	public Move makeMove(String oponentName) {
		return getMoveByPercent(getWeightedPercentMean(oponentName, 1));
	}

	public void addOponentMove(Profile profileOponent, Move move) {
		addOponentMove(profileOponent.getName(), move);
	}

	public void addOponentMove(String oponentName, Move move) {
		createOponentMovesListIfNotExist(oponentName);
		addOponentMoveInList(oponentName, move);
	}

	public LinkedList<Move> getOponentMoves(Profile profileOponent) {
		return getOponentMoves(profileOponent.getName());
	}

	public LinkedList<Move> getOponentMoves(String oponentName) {
		createOponentMovesListIfNotExist(oponentName);
		return this.oponentMovesMap.get(oponentName);
	}

	public Move getMoveByOponentMoves(Profile profileOponent) {
		return getMoveByOponentMoves(profileOponent.getName());
	}

	public Move getMoveByOponentMoves(String oponentName) {
		System.out.println("NAME"+oponentName);
		LinkedList<Move> oponentMovesList = getOponentMoves(oponentName);

		int numberRock = 0;
		int numberPaper = 0;
		int numberScissors = 0;

		for (Move move : oponentMovesList) {
			if (move.equals(Move.ROCK)) {
				numberRock++;
			} else if (move.equals(Move.PAPER)) {
				numberPaper++;
			} else {
				numberScissors++;
			}
		}

		int numberLargest = Math.max(numberRock, numberPaper);
		numberLargest = Math.max(numberLargest, numberScissors);

		if (numberLargest == numberRock) {
			return Move.ROCK;
		} else if (numberLargest == numberPaper) {
			return Move.PAPER;
		} else {
			return Move.SCISSORS;
		}
	}

	public Move getMoveByPreference() {
		Random random = new Random();

		int randomNumber = random.nextInt(100) + 1;

		int numberRock = this.movePercent.get(Move.ROCK);
		int numberPaper = numberRock + this.movePercent.get(Move.PAPER);

		if (randomNumber <= numberRock) {
			return Move.ROCK;
		} else if (randomNumber <= numberPaper) {
			return Move.PAPER;
		} else {
			return Move.SCISSORS;
		}
	}
	
	
	public Move getMoveByPercent(HashMap<Move, Integer> percent) {
		Random random = new Random();

		int randomNumber = random.nextInt(100) + 1;

		int numberRock = percent.get(Move.ROCK);
		int numberPaper = numberRock +percent.get(Move.PAPER);

		if (randomNumber <= numberRock) {
			return Move.ROCK;
		} else if (randomNumber <= numberPaper) {
			return Move.PAPER;
		} else {
			return Move.SCISSORS;
		}
	}
	
	public HashMap<Move, Integer> getWeightedPercentMean(String oponentName, float oponentWeight){
		LinkedList<Move> oponentMovesList = getOponentMoves(oponentName);
		if(oponentMovesList == null || oponentMovesList.size() == 0){
			return this.movePercent;
		}
		
		int total = oponentMovesList.size();
		int numberRock = 0;
		int numberPaper = 0;
		int numberScissors = 0;

		for (Move move : oponentMovesList) {
			if (move.equals(Move.ROCK)) {
				numberRock++;
			} else if (move.equals(Move.PAPER)) {
				numberPaper++;
			} else {
				numberScissors++;
			}
		}
		
		HashMap<Move, Integer> percent = new HashMap<Move, Integer>();
		System.out.println(total);
		System.out.println(numberPaper);
		System.out.println(total);
		percent.put(Move.PAPER,(int) ((this.movePercent.get(Move.PAPER) + 
				oponentWeight *(100*numberRock/total))/(oponentWeight+1)));
		percent.put(Move.SCISSORS,(int) ((this.movePercent.get(Move.SCISSORS) + 
				oponentWeight *(100*numberPaper/total))/(oponentWeight+1)));
		percent.put(Move.ROCK,(int) ((this.movePercent.get(Move.ROCK) + 
				oponentWeight *(100*numberScissors/total))/(oponentWeight+1)));
		
		
		return percent;
	}

	public void setPreference(int rock, int paper, int scissors) {
		this.movePercent.put(Move.ROCK, rock);
		this.movePercent.put(Move.PAPER, paper);
		this.movePercent.put(Move.SCISSORS, scissors);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Profile) {
			Profile another = (Profile) obj;
			return this.name.equals(another.name);
		}
		return false;
	}

	@Override
	public String toString() {
		return this.name;
	}

	private void createOponentMovesListIfNotExist(String oponentName) {
		if (!this.oponentMovesMap.containsKey(oponentName)) {
			this.oponentMovesMap.put(oponentName, new LinkedList<Move>());
		}
	}

	private void addOponentMoveInList(String oponentName, Profile.Move move) {
		this.oponentMovesMap.get(oponentName).addLast(move);
	}

	public static LinkedList<Profile> getAll() {
		return ProfileDAO.getInstance().getAll();
	}

	public static LinkedList<Profile> getAllActive() {
		LinkedList<Profile> activeProfileList = new LinkedList<Profile>();

		for (Profile profile : getAll()) {
			if (profile.isActive()) {
				activeProfileList.addLast(profile);
			}
		}

		return activeProfileList;
	}

	public static int count() {
		return ProfileDAO.getInstance().getAll().size();
	}

	public static Profile findByName(String name) {
		for (Profile profile : getAll()) {
			if (profile.getName().equals(name)) {
				return profile;
			}
		}

		return null;
	}

	public interface ProfileListener {
		public void onSave(Profile profile);

		public void onDestroy(Profile profile);
	}

	private static LinkedList<ProfileListener> listenerList = new LinkedList<ProfileListener>();

	public static void addListener(ProfileListener listener) {
		listenerList.add(listener);
	}

	public static void removeListener(ProfileListener listener) {
		listenerList.remove(listener);
	}

	public static void removeAllProfileListener() {
		listenerList.clear();
	}

	private static void notifyOnSave(Profile profile) {
		for (ProfileListener listener : listenerList) {
			listener.onSave(profile);
		}
	}

	private static void notifyOnDestroy(Profile profile) {
		for (ProfileListener listener : listenerList) {
			listener.onDestroy(profile);
		}
	}
}
