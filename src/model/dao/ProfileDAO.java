package model.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

import model.Profile;

public class ProfileDAO {

	private static ProfileDAO instance;

	public synchronized static ProfileDAO getInstance() {
		if (instance == null) {
			instance = new ProfileDAO();
		}
		return instance;
	}

	private LinkedList<Profile> profileList;
	private boolean useFile;

	private ProfileDAO() {
		restartDAO();
	}

	public synchronized void save(Profile profile) {
		if (this.profileList.contains(profile)) {
			this.profileList.remove(profile);
		}
		this.profileList.addLast(profile);
		saveProfileList();
	}

	public synchronized void destroy(Profile profile) {
		this.profileList.remove(profile);
	}

	public synchronized LinkedList<Profile> getAll() {
		loadProfileList();
		return this.profileList;
	}

	public synchronized void restartDAO() {
		this.profileList = new LinkedList<Profile>();
		this.useFile = true;
	}

	public synchronized void setUseFile(boolean useFile) {
		this.useFile = useFile;
	}

	private synchronized void saveProfileList() {
		if (!this.useFile) {
			return;
		}
		try {
			File file = new File("./profiles.jkp");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream("./profiles.jkp");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(this.profileList);

			oos.close();
		} catch (Exception e) {
			System.out.println("Error {saveProfileList()}: " + e.getMessage());
		}
	}

	private synchronized void loadProfileList() {
		if (!this.useFile) {
			return;
		}
		try {
			File file = new File("./profiles.jkp");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);

			this.profileList = (LinkedList<Profile>) ois.readObject();

			ois.close();
		} catch (Exception e) {
			System.out.println("Error {loadProfileList()}: " + e.getMessage());
		}
	}
}
