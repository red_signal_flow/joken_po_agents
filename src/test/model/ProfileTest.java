package test.model;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;

import model.Profile;
import model.Profile.Move;
import model.Profile.ProfileListener;
import model.dao.ProfileDAO;

import org.junit.Before;
import org.junit.Test;

public class ProfileTest {

	@Before
	public void setup() {
		ProfileDAO.getInstance().restartDAO();
		ProfileDAO.getInstance().setUseFile(false);
		Profile.removeAllProfileListener();
	}

	@Test
	public void createProfile() throws Exception {
		Profile profile = new Profile();
		profile.setName("New Category");
		profile.save();

		assertEquals(profile, Profile.getAll().get(0));
	}

	@Test
	public void createMoreProfiles() throws Exception {
		Profile profile = new Profile();
		profile.setName("1 Category");
		profile.save();
		profile = new Profile();
		profile.setName("2 Category");
		profile.save();
		profile = new Profile();
		profile.setName("3 Category");
		profile.save();

		assertEquals(3, Profile.count());
	}

	@Test
	public void findCategoryByName() throws Exception {
		String name = "New Category";
		Profile profile = new Profile();
		profile.setName(name);
		profile.save();

		assertEquals(profile, Profile.findByName(name));
	}

	@Test
	public void destroyProfiles() throws Exception {
		Profile profile = new Profile();
		profile.setName("New Category");
		profile.save();
		profile.destroy();

		assertEquals(0, Profile.count());
	}

	@Test
	public void callListenerWhenSaveProfile() throws Exception {
		TestProfileListener listener = new TestProfileListener();
		Profile.addListener(listener);

		Profile profile = new Profile();
		profile.setName("New Category");
		profile.save();
		profile.save();

		assertEquals(2, listener.getCountProfilesSaved());
	}

	@Test
	public void callListenerWhenDestroyProfile() throws Exception {
		TestProfileListener listener = new TestProfileListener();
		Profile.addListener(listener);

		Profile profile = new Profile();
		profile.setName("New Category");
		profile.destroy();
		profile.destroy();

		assertEquals(2, listener.getCountProfilesDestroyed());
	}

	@Test
	public void addOponentMoveInList() throws Exception {
		Profile profile = new Profile("profile");
		Profile profileOponent = new Profile("profile oponent");
		LinkedList<Profile.Move> oponentMovesList = new LinkedList<Profile.Move>();

		oponentMovesList.addLast(Profile.Move.ROCK);
		oponentMovesList.addLast(Profile.Move.PAPER);
		profile.addOponentMove(profileOponent, Profile.Move.ROCK);
		profile.addOponentMove(profileOponent, Profile.Move.PAPER);

		assertEquals(oponentMovesList, profile.getOponentMoves(profileOponent));
	}

	@Test
	public void moveByPreferenceIsRock() throws Exception {
		Profile profile = new Profile("profile");
		profile.setPreference(100, 0, 0);

		assertEquals(Move.ROCK, profile.getMoveByPreference());
	}

	@Test
	public void moveByPreferenceIsPaper() throws Exception {
		Profile profile = new Profile("profile");
		profile.setPreference(0, 100, 0);

		assertEquals(Move.PAPER, profile.getMoveByPreference());
	}

	@Test
	public void moveByPreferenceIsScissors() throws Exception {
		Profile profile = new Profile("profile");
		profile.setPreference(0, 0, 100);

		assertEquals(Move.SCISSORS, profile.getMoveByPreference());
	}

	@Test
	public void moveByOponentMovesIsRock() throws Exception {
		Profile profile = new Profile("profile");
		Profile profileOponent = new Profile("profile oponent");

		profile.addOponentMove(profileOponent, Profile.Move.ROCK);
		profile.addOponentMove(profileOponent, Profile.Move.ROCK);
		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
		profile.addOponentMove(profileOponent, Profile.Move.SCISSORS);

		assertEquals(Move.ROCK, profile.getMoveByOponentMoves(profileOponent));
	}
	
//	@Test
//	public void moveByWeightPercentageIsMostlyScissors() throws Exception {
//		Profile profile = new Profile("profile");
//		Profile profileOponent = new Profile("profile oponent");
//		profile.setPreference(20, 20, 60);
//		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
//		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
//		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
//		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
//
//		assertEquals(Move.SCISSORS, profile.getMoveByPercent(profile.getWeightedPercentMean(profileOponent.getName(), 1)));
//	}

	@Test
	public void moveByOponentMovesIsPaper() throws Exception {
		Profile profile = new Profile("profile");
		Profile profileOponent = new Profile("profile oponent");

		profile.addOponentMove(profileOponent, Profile.Move.ROCK);
		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
		profile.addOponentMove(profileOponent, Profile.Move.SCISSORS);

		assertEquals(Move.PAPER, profile.getMoveByOponentMoves(profileOponent));
	}

	@Test
	public void moveByOponentMovesIsScissors() throws Exception {
		Profile profile = new Profile("profile");
		Profile profileOponent = new Profile("profile oponent");

		profile.addOponentMove(profileOponent, Profile.Move.ROCK);
		profile.addOponentMove(profileOponent, Profile.Move.PAPER);
		profile.addOponentMove(profileOponent, Profile.Move.SCISSORS);
		profile.addOponentMove(profileOponent, Profile.Move.SCISSORS);

		assertEquals(Move.SCISSORS,
				profile.getMoveByOponentMoves(profileOponent));
	}

	private class TestProfileListener implements ProfileListener {

		private int countProfilesSaved;
		private int countProfilesDestroyed;

		public TestProfileListener() {
			this.countProfilesSaved = 0;
			this.countProfilesDestroyed = 0;
		}

		@Override
		public void onSave(Profile profile) {
			this.countProfilesSaved++;
		}

		@Override
		public void onDestroy(Profile profile) {
			this.countProfilesDestroyed++;
		}

		public int getCountProfilesSaved() {
			return countProfilesSaved;
		}

		public int getCountProfilesDestroyed() {
			return countProfilesDestroyed;
		}
	}
}
