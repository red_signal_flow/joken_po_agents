package view;

import java.awt.event.ActionEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import agent.ManagerTournament;
import agent.ManagerTournament.ManagerTournamentListener;
import model.Profile;

public class DuelPane extends javax.swing.JPanel implements
		ManagerTournamentListener {

	private static final long serialVersionUID = -1863253517471032949L;

	private JButton buttonSetOponentOne;
	private JButton buttonSetOponentTwo;
	private JButton buttonPlay;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JScrollPane jScrollPane1;
	private JLabel labelOponentOneMove;
	private JLabel labelOponentOneName;
	private JLabel labelOponentTwoMove;
	private JLabel labelOponentTwoName;
	private JLabel labelOponentWinner;
	private JList<Profile> listProfile;

	private DefaultListModel<Profile> listModel;

	private Profile oponentOne;
	private Profile oponentTwo;

	public DuelPane() {
		initComponents();
		updateListModel();
		ManagerTournament.setListener(this);
	}

	@Override
	public void onWinner(int result) {
		System.out.println("Result: " + result);
		if (result == ManagerTournament.WIN_PLAYER_ONE) {
			labelOponentWinner.setText(labelOponentOneName.getText());
		} else if (result == ManagerTournament.WIN_PLAYER_TWO) {
			labelOponentWinner.setText(labelOponentTwoName.getText());
		} else {
			labelOponentWinner.setText("Empate");
		}
	}

	@Override
	public void onMovePlayerOne(String move) {
		this.labelOponentOneMove.setText(move);
	}

	@Override
	public void onMovePlayerTwo(String move) {
		this.labelOponentTwoMove.setText(move);
	}

	private void buttonPlayActionPerformed(ActionEvent evt) {
		if (!labelOponentOneName.getText().isEmpty()
				&& !labelOponentTwoName.getText().isEmpty()) {
			ManagerTournament.play(labelOponentOneName.getText(),
					labelOponentTwoName.getText());
		}
	}

	private void buttonSetOponentOneActionPerformed(ActionEvent evt) {
		Profile profile = getListSelectedProfile();
		if (profile != null) {
			if (profile != this.oponentTwo) {
				this.oponentOne = profile;
				this.labelOponentOneName.setText(this.oponentOne.getName());
			} else {
				JOptionPane.showMessageDialog(this,
						"Selecione outro nome, esse ja foi selecionado",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void buttonSetOponentTwoActionPerformed(ActionEvent evt) {
		Profile profile = getListSelectedProfile();
		if (profile != null) {
			if (profile != this.oponentOne) {
				this.oponentTwo = profile;
				this.labelOponentTwoName.setText(this.oponentTwo.getName());
			} else {
				JOptionPane.showMessageDialog(this,
						"Selecione outro nome, esse ja foi selecionado",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void updateListModel() {
		this.listModel.clear();
		for (Profile profile : Profile.getAllActive()) {
			this.listModel.addElement(profile);
		}
	}

	private Profile getListSelectedProfile() {
		int index = this.listProfile.getSelectedIndex();
		if (index != -1) {
			Profile profile = this.listModel.get(index);
			return profile;
		} else {
			JOptionPane.showMessageDialog(this, "Selecione um nome na lista",
					"Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		listModel = new DefaultListModel<Profile>();
		listProfile = new javax.swing.JList<Profile>();
		buttonSetOponentOne = new javax.swing.JButton();
		buttonSetOponentTwo = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		labelOponentOneName = new javax.swing.JLabel();
		labelOponentTwoName = new javax.swing.JLabel();
		labelOponentOneMove = new javax.swing.JLabel();
		labelOponentTwoMove = new javax.swing.JLabel();
		buttonPlay = new javax.swing.JButton();
		jLabel3 = new javax.swing.JLabel();
		labelOponentWinner = new javax.swing.JLabel();

		listProfile.setModel(listModel);
		jScrollPane1.setViewportView(listProfile);

		buttonSetOponentOne.setText(">>");

		buttonSetOponentTwo.setText(">>");

		jLabel1.setText("Oponente:");

		jLabel2.setText("Oponente:");

		buttonSetOponentOne
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						buttonSetOponentOneActionPerformed(evt);
					}
				});

		buttonSetOponentTwo
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						buttonSetOponentTwoActionPerformed(evt);
					}
				});


		buttonPlay.setText("Jogar");
		buttonPlay.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonPlayActionPerformed(evt);
			}
		});

		jLabel3.setText("Vencedor:");

		

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jScrollPane1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										180,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		buttonSetOponentOne)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jLabel2)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(
																						labelOponentOneMove,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						145,
																						Short.MAX_VALUE)
																				.addComponent(
																						labelOponentOneName,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		buttonSetOponentTwo)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jLabel1)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(
																						labelOponentTwoName,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addComponent(
																										labelOponentTwoMove,
																										javax.swing.GroupLayout.PREFERRED_SIZE,
																										82,
																										javax.swing.GroupLayout.PREFERRED_SIZE)
																								.addGap(0,
																										0,
																										Short.MAX_VALUE))))
												.addGroup(
														layout.createSequentialGroup()
																.addGap(0,
																		0,
																		Short.MAX_VALUE)
																.addComponent(
																		buttonPlay,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		75,
																		javax.swing.GroupLayout.PREFERRED_SIZE))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jLabel3)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		labelOponentWinner)
																.addGap(0,
																		0,
																		Short.MAX_VALUE)))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														jScrollPane1,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														276, Short.MAX_VALUE)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						buttonSetOponentOne)
																				.addComponent(
																						jLabel2)
																				.addComponent(
																						labelOponentOneName))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		labelOponentOneMove)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						jLabel3)
																				.addComponent(
																						labelOponentWinner))
																.addGap(43, 43,
																		43)
																.addComponent(
																		labelOponentTwoMove)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						buttonSetOponentTwo)
																				.addComponent(
																						jLabel1)
																				.addComponent(
																						labelOponentTwoName))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		buttonPlay)))
								.addContainerGap()));
	}
}
