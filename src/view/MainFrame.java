package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -6399886456682347905L;

	public MainFrame() {
		super("Joken po");

		initializeScreen();
	}

	public void initializeScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(new DuelPane());
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
