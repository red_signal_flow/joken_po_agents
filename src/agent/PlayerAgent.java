package agent;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import model.Profile;

public class PlayerAgent extends Agent {

	private static final long serialVersionUID = 1L;

	Profile profile;

	protected void setup() {
		profile = Profile.findByName(this.getLocalName());
		if (profile == null) {
			profile = new Profile(this.getLocalName());
		} else {
			profile.setActive(true);
		}
		profile.save();
		registerToPoker();
		addBehaviour(new PlayTurns());

	}

	private class GETMessage extends CyclicBehaviour {

		private static final long serialVersionUID = -5151433161792373064L;

		public void action() {
			MessageTemplate messageTemplate = MessageTemplate
					.MatchPerformative(ACLMessage.CFP);
			ACLMessage message = myAgent.receive(messageTemplate);

			System.out.println("getmessages-----");

			if (message != null) {
				System.out.println("HERE [" + message.getContent() + "]");
			} else {
				block();
			}
		}
	}

	private class PlayTurns extends CyclicBehaviour {

		private static final long serialVersionUID = 1681809211196002809L;

		String currentOpponent = null;

		public void action() {
			MessageTemplate messageTemplate = MessageTemplate
					.MatchPerformative(ACLMessage.CFP);
			ACLMessage message = myAgent.receive(messageTemplate);
			String content, replyContent;

			if (message != null) {
				content = message.getContent();

				if (content.equals("play")) {
					replyContent = convertToString(profile
							.makeMove(currentOpponent));
					// create new message to manager
					ACLMessage m = new ACLMessage(ACLMessage.CFP);
					m.addReceiver(message.getSender());
					m.setContent(replyContent);
					send(m);
				} else {

				}

				/*
				 * if (currentOpponent == null){ currentOpponent =
				 * message.getContent();
				 * System.out.println("CURRENT_OPPONET"+currentOpponent); } if
				 * (content.equalsIgnoreCase("nextMatch")) { currentOpponent =
				 * null; }
				 * 
				 * if (content.equals("play")){ replyContent =
				 * convertToString(profile.makeMove(currentOpponent)); }
				 * 
				 * if (content.equals("ROCK") || content.equals("PAPER") ||
				 * content.equals("SCISSORS")) {
				 * profile.addOponentMove(currentOpponent,
				 * analizeMove(message.getContent())); }
				 */
				// manda a mensagem para quem enviou

			} else {
				block();
			}
		}

		public String convertToString(Profile.Move move) {
			String result = null;
			if (Profile.Move.ROCK.equals(move))
				result = "ROCK";
			else if (Profile.Move.SCISSORS.equals(move))
				result = "SCISSORS";
			if (Profile.Move.PAPER.equals(move)) {
				result = "PAPER";
			}
			return result;
		}

		public Profile.Move analizeMove(String message) {
			Profile.Move move;
			if (message.equals("ROCK")) {
				move = Profile.Move.ROCK;
			} else if (message.equals("PAPER")) {
				move = Profile.Move.PAPER;
			} else {
				move = Profile.Move.SCISSORS;
			}

			return move;
		}

	}

	protected void takeDown() {
		// kill agent
		profile.setActive(false);
		profile.save();
		doDelete();
	}

	public void registerToPoker() {
		DFAgentDescription template = new DFAgentDescription();
		template.setName(getAID());
		ServiceDescription service = new ServiceDescription();

		service.setType("player");
		service.setName("player-start-play");

		template.addServices(service);
		try {
			DFService.register(this, template);
			System.out.println("Player [" + getName() + "] was Registered");
		} catch (FIPAException e) {
			System.out.println("Error try to regiter new player"
					+ e.getMessage());
		}
	}
}
