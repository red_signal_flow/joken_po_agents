package agent;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import view.MainFrame;

public class ManagerTournament extends Agent {

	public static final int WIN_DRAW = 0;
	public static final int WIN_PLAYER_ONE = 1;
	public static final int WIN_PLAYER_TWO = 2;;

	public interface ManagerTournamentListener {
		public void onWinner(int result);

		public void onMovePlayerOne(String move);

		public void onMovePlayerTwo(String move);
	}

	private static final long serialVersionUID = 1L;

	private static final String SCREEN_TYPE = "Nimbus";

	private static ManagerTournamentListener listener;

	public static void play(String onePlayerName, String twoPlayerName) {
		synchronized (canPlay) {
			canPlay = true;
			playerOneName = onePlayerName;
			playerTwoName = twoPlayerName;
		}
	}

	public static void setListener(ManagerTournamentListener listenerSetter) {
		listener = listenerSetter;
	}

	private static void notifyOnWinner(int winnerResult) {
		if (listener != null) {
			listener.onWinner(winnerResult);
		}
	}

	private static void notifyOnMovePlayerOne(String move) {
		if (listener != null) {
			listener.onMovePlayerOne(move);
		}
	}

	private static void notifyOnMovePlayerTwo(String move) {
		if (listener != null) {
			listener.onMovePlayerTwo(move);
		}
	}

	private static Boolean canPlay = false;
	private static String playerOneName = "";
	private static String playerTwoName = "";

	public void setup() {
		addBehaviour(new ManagerGame());
	}

	public void takeDown() {

	}

	private class ManagerGame extends CyclicBehaviour {

		private static final long serialVersionUID = -5914408510986239838L;

		ArrayList<AID> players;
		int step = 0;

		int aux = 0;

		ACLMessage messagePlayer1 = null;
		ACLMessage messagePlayer2 = null;
		String messageMovePlayer1 = null;
		String messageMovePlayer2 = null;

		public ManagerGame() {
			players = new ArrayList<AID>();
			initializeScreen();
		}

		public void action() {

			synchronized (canPlay) {
				if (!canPlay) {
					step = 0;
					return;
				}
			}

			switch (step) {
			case 0:
				initializeAttributes();
				waitAndRegisterPlayersOfDF();
				break;
			case 1:
				selectPlayers();
				step = 2;
				break;
			case 2:
				askForMove();
				step = 3;
			case 3:
				receiveMove();
				break;
			}
		}

		private void initializeAttributes() {
			players.clear();

			aux = 0;

			messagePlayer1 = null;
			messagePlayer2 = null;
		}

		private int winner(String m1, String m2) {
			if (m1.equals("ROCK")) {
				if (m2.equals("PAPER")) {
					return 2;
				} else if (m2.equals("SCISSORS")) {
					return 1;
				}
			} else if (m1.equals("SCISSORS")) {
				if (m2.equals("PAPER")) {
					return 1;
				} else if (m2.equals("ROCK")) {
					return 2;
				}
			} else {
				if (m2.equals("ROCK")) {
					return 1;
				} else if (m2.equals("SCISSORS")) {
					return 2;
				}
			}
			return 0;
		}

		private void receiveMove() {
			MessageTemplate messageTemplate = MessageTemplate
					.MatchPerformative(ACLMessage.CFP);
			ACLMessage message = myAgent.receive(messageTemplate);

			if (message != null) {
				System.out.println("Message=" + message.getContent()
						+ "sender=" + message.getSender());

				aux++;

				if (aux == 1) {
					messageMovePlayer1 = message.getContent();
				}

				if (aux >= 2) {
					step = 4;
					aux = 0;

					messageMovePlayer2 = message.getContent();

					int winnerResult = winner(messageMovePlayer1,
							messageMovePlayer2);

					notifyOnMovePlayerOne(messageMovePlayer1);
					notifyOnMovePlayerTwo(messageMovePlayer2);
					notifyOnWinner(winnerResult);
					synchronized (canPlay) {
						canPlay = false;
					}
				}
			} else {
				block();
			}

		}

		private void askForMove() {
			messagePlayer1.setContent("play");
			messagePlayer2.setContent("play");
			send(messagePlayer1);
			send(messagePlayer2);
		}

		private void selectPlayers() {
			AID player1 = players.get(0);
			AID player2 = players.get(1);

			if (messagePlayer1 == null) {
				messagePlayer1 = new ACLMessage(ACLMessage.CFP);
				messagePlayer1.addReceiver(player1);
				messagePlayer1.setContent(player2.getLocalName());
			}

			if (messagePlayer2 == null) {
				messagePlayer2 = new ACLMessage(ACLMessage.CFP);
				messagePlayer2.addReceiver(player2);
				messagePlayer2.setContent(player1.getLocalName());
			}
		}

		private void waitAndRegisterPlayersOfDF() {
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("player");
			template.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(getAgent(),
						template);

				for (int x = 0; x < result.length; x++) {
					System.out.println("RESULT[" + result[x].getName() + "]");
					System.out.println(result[x].getName().getLocalName());
					if (result[x].getName().getLocalName()
							.equals(playerOneName)
							|| result[x].getName().getLocalName()
									.equals(playerTwoName)) {
						players.add(result[x].getName());
					}

					if (players.size() == 2) {
						break;
					}
				}
				step = 1;

			} catch (FIPAException e) {
				e.printStackTrace();
			}

		}

	}

	private static void initializeScreen() {
		defineScreenType();
		startScreenThread();
	}

	private static void startScreenThread() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}

	private static void defineScreenType() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if (SCREEN_TYPE.equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		}
	}
}
